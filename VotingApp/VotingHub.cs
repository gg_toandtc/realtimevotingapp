﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace VotingApp
{
    public class VotingHub : Hub
    {
        public void Hello()
        {
            Clients.All.hello();
        }
    }
}